package controllers

import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms.{ mapping, nonEmptyText, email }

trait Secured {
  // we use the email for the username in this example
  def username(request: RequestHeader) = request.session.get("email")

  def ifUnauthorized(request: RequestHeader) = {
    println("Unauthorized Access Baby!... Redirecting to log yo ass in!")
    Results.Redirect(routes.Application.login)
  }

  def Auth(f: => String => Request[AnyContent] => Result) = {
    Security.Authenticated(username, ifUnauthorized) { 
      user => Action(request => f(user)(request))
    }
  }
    
}

case class User(email: String, password: String)

object Application extends Controller with Secured {
  
  def index = Auth { u => implicit request =>
    println(request.session)
    Ok("You can only view this if you are logged in! \n\nYou must be logged in :=)")
  }
  
  val loginForm: Form[User] = Form(
      mapping(
    	"email" 	-> email,
    	"password"	-> nonEmptyText
      ) 
      (User.apply) 
      (User.unapply) 
      verifying (
        "Invalid credentials, please try again!",
        f => (f.email == "admin@admin.com" && f.password == "pass")
      )
  )
  
  // !! - implicits must be explicityly written when using flashes
    
  def login = Action { implicit request =>
    println(request.session)
    Ok(views.html.login(loginForm))
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formErrors => BadRequest(views.html.login(formErrors)),
      user => Redirect(routes.Application.index).withSession("email" -> user.email)
    )
  }
   
  def logout = Action {
    Redirect(routes.Application.login).withNewSession.flashing("success" -> "You've been logged out")
  }
  
}